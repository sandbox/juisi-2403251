(function($){

    /**
     * Add an extra function to the Drupal ajax object
     * which allows us to trigger an ajax response without
     * an element that triggers it.
     */
    Drupal.ajax.prototype.specifiedResponse = function() {
        var ajax = this;

        // Do not perform another ajax command if one is already in progress.
        if (ajax.ajaxing) {
            return false;
        }

        try {
            $.ajax(ajax.options);
        }
        catch (err) {
            alert('An error occurred while attempting to process ' + ajax.options.url);
            return false;
        }

        return false;
    };

    /**
     * Define a custom ajax action not associated with an element.
     */
    var custom_settings = {};
    custom_settings.url = '/commerce_ajax_checkout/ajax/refresh-elements';
    custom_settings.event = 'onload';
    custom_settings.keypress = false;
    custom_settings.prevent = false;
    Drupal.ajax['custom_ajax_action'] = new Drupal.ajax(null, $(document.body), custom_settings);

    /**
     * Define a point to trigger our custom actions. e.g. on page load.
     */
    $(document).ready(function() {
        Drupal.ajax['custom_ajax_action'].specifiedResponse();
    });

})(jQuery);                